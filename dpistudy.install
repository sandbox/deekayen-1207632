<?php

/**
 * Implementation of hook_schema().
 *
 * @return array
 */
function dpistudy_schema() {
  $schema = array();

  $schema['dpistudy_answers'] = array(
    'description' => t('Answer choices to questions on the DPI questionnaire.'),
    'fields' => array(
      'parent' => array(
        'description' => t('{dpistudy_questions}.pkid'),
        'type' => 'int',
        'size' => 'medium',
        'unsigned' => false,
        'not null' => true,
        'default' => 0
      ),
      'weight' => array(
        'description' => t('The order in which to display the answer choices. Smaller values should appear before larger values.'),
        'type' => 'int',
        'size' => 'small',
        'unsigned' => false,
        'not null' => true,
        'default' => 0
      ),
      'answer' => array(
        'description' => t('Text of the answer choice, displayed next to a radio button or checkbox for form submission.'),
        'type' => 'varchar',
        'not null' => true,
        'default' => '',
        'length' => 250
      )
    )
  );

  $schema['dpistudy_keys'] = array(
    'description' => t('Invitation keys so people who received an invitation have a way to be recognized.'),
    'fields' => array(
      'invite_key' => array(
        'description' => t('Short string to identify a participant as a member of a certain group when they respond to a specially constructed invitation.'),
        'type' => 'varchar',
        'not null' => true,
        'default' => '',
        'length' => 50
      ),
      'notes' => array(
        'description' => t('Description of what the key should be used for. Only useful as a reminder to admins.'),
        'type' => 'varchar',
        'not null' => true,
        'default' => '',
        'length' => 255
      )
    ),
    'primary key' => array('invite_key')
  );

  $schema['dpistudy_login'] = array(
    'description' => t(''),
    'fields' => array(
      'sessid' => array(
        'description' => t('MD5 hash of the session cookie, created by PHP, used to recognize a visitor across page views.'),
        'type' => 'char',
        'length' => 32,
        'not null' => true,
        'default' => ''
      ),
      'invite_key' => array(
        'description' => t('Invitation key from {dpistudy_keys}.invite_key.'),
        'type' => 'varchar',
        'not null' => true,
        'default' => '',
        'length' => 50
      ),
      'agent' => array(
        'description' => t('Web browser identifier. May help define bots from people.'),
        'type' => 'varchar',
        'not null' => true,
        'default' => '',
        'length' => 250
      ),
      'referer' => array(
        'description' => t('Where the user came from. May be useful for identifying people that would re-type the URL instead of clicking from an email invite.'),
        'type' => 'varchar',
        'not null' => true,
        'default' => '',
        'length' => 4096
      ),
      'irb_php_timer' => array(
        'description' => t('Value of the time spent between when the IRB consent page was loaded versus when it was submitted according to the difference in PHP\'s microtime() function.'),
        'type' => 'float',
        'not null' => true,
        'default' => 0
      ),
      'irb_apache_timer' => array(
        'description' => t('Value of the time spent between page loads according to the difference in timestamps from the Date HTTP headers sent between viewing pages when viewing the IRB consent page.'),
        'type' => 'int',
        'size' => 'medium',
        'unsigned' => false,
        'not null' => true,
        'default' => 0
      ),
      'irb_js_timer' => array(
        'description' => t('Value of a form-submitted variable set by a javascript function that starts incrementing by 1 from 0 as soon as each page finishes loading in its entirety when viewing the IRB consent page. Values here depend on the client browser both supporting and allowing javascript while viewing each page.'),
        'type' => 'int',
        'size' => 'medium',
        'unsigned' => false,
        'not null' => true,
        'default' => 0
      )
    ),
    'primary key' => array('sessid', 'invite_key')
  );

  $schema['dpistudy_questions'] = array(
    'description' => t('Questions asked as part of the instructional intervention.'),
    'fields' => array(
      'pkid' => array(
        'description' => t('Identifier to keep track of relations between this table and {dpistudy_responses}.question'),
        'type' => 'serial',
        'size' => 'medium',
        'unsigned' => false,
        'not null' => true
      ),
      'weight' => array(
        'description' => t('The order in which the question should appear in the DPI questionnaire. Questions with smaller weights should appear before larger weights. The default dataset loaded by the module install file has weights which correspond to the question numbers in the original DPI questionnaire by Kelsey Henderson.'),
        'type' => 'int',
        'size' => 'small',
        'unsigned' => false,
        'not null' => true,
        'default' => 0
      ),
      'qtype' => array(
        'description' => t('Defines whether a question should display radio buttons for a single response or checkboxes to answer with more than one choice. Equivalent to MySQL enum since it will only have "radio" or "checkbox" as values as a format for answering the question.'),
        'type' => 'varchar',
        'not null' => true,
        'default' => 'radio',
        'length' => 8
      ),
      'title' => array(
        'description' => t('The prompt or text of the question.'),
        'type' => 'varchar',
        'not null' => true,
        'default' => '',
        'length' => 250
      )
    ),
    'primary key' => array('pkid'),
    'indexes' => array(
      'weight' => array('weight')
    )
  );

  $schema['dpistudy_responses'] = array(
    'description' => t('Possible answers to the questions asked during the instructional intervention from {dpistudy_questions}.'),
    'fields' => array(
      'sessid' => array(
        'description' => t('See {dpistudy_login}.sessid.'),
        'type' => 'char',
        'not null' => true,
        'default' => '',
        'length' => 32
      ),
      'question' => array(
        'description' => t('See {dpistudy_questions}.pkid.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => true,
        'default' => 0
      ),
      'answer' => array(
        'description' => t('See {dpistudy_answers}.weight.'),
        'type' => 'int',
        'size' => 'tiny',
        'not null' => true,
        'default' => 9
      ),
      'php_timer' => array(
        'description' => t('Value of the time spent between page loads during the instructional intervention according to the difference in PHP\'s microtime() function.'),
        'type' => 'float',
        'not null' => true,
        'default' => 0
      ),
      'apache_timer' => array(
        'description' => t('Value of the time spent between page loads according to the difference in timestamps from the Date HTTP headers sent between viewing pages during the instructional intervention.'),
        'type' => 'int',
        'size' => 'medium',
        'unsigned' => false,
        'not null' => true,
        'default' => 0
      ),
      'js_timer' => array(
        'description' => t('Value of a form-submitted variable set by a javascript function that starts incrementing by 1 from 0 as soon as each page finishes loading in its entirety during the instructional intervention. Values here depend on the client browser both supporting and allowing javascript while viewing each page.'),
        'type' => 'int',
        'size' => 'medium',
        'unsigned' => false,
        'not null' => true,
        'default' => 0
      )
    ),
    'primary key' => array('sessid', 'question')
  );

  return $schema;
}

/**
 * Implementation of hook_install().
 *
 * Loads the DPI questionnaire and some default invitation keys.
 */
function dpistudy_install() {
  drupal_install_schema('dpistudy');

  db_query("UPDATE {permission} SET perm = 'access dpistudy, access content' WHERE rid = 1");

  db_query("INSERT INTO {dpistudy_answers} (parent, weight, answer) VALUES
(1, 1, 'at no time during the week.'),
(1, 2, 'weekly.'),
(1, 3, '2-3 days per week.'),
(1, 4, 'daily.'),
(1, 5, 'more than 3 times daily.'),
(2, 1, 'at no time during the week.'),
(2, 2, 'weekly.'),
(2, 3, '2-3 days per week.'),
(2, 4, 'daily.'),
(2, 5, 'more than 3 times daily.'),
(3, 1, 'at no time during the week.'),
(3, 2, 'weekly.'),
(3, 3, '2-3 days per week.'),
(3, 4, 'daily.'),
(3, 5, 'more than 3 times daily.'),
(4, 1, 'never.'),
(4, 2, 'monthly.'),
(4, 3, 'weekly.'),
(4, 4, 'daily.'),
(4, 5, 'more than 3 times daily.'),
(5, 1, 'never.'),
(5, 2, 'monthly.'),
(5, 3, 'weekly.'),
(5, 4, 'daily.'),
(5, 5, 'more than 3 times daily.'),
(6, 1, 'never.'),
(6, 2, 'monthly.'),
(6, 3, 'weekly.'),
(6, 4, 'daily.'),
(6, 5, 'more than 3 times daily.'),
(7, 1, 'at no time during the year.'),
(7, 2, 'annually.'),
(7, 3, '2-3 times per year.'),
(7, 4, 'monthly.'),
(7, 5, 'more than 3 times per month.'),
(8, 1, 'at no time during the week.'),
(8, 2, 'weekly.'),
(8, 3, '2-3 days per week.'),
(8, 4, 'daily.'),
(8, 5, 'more than 3 times daily.'),
(9, 1, 'at no time during the week.'),
(9, 2, 'weekly.'),
(9, 3, '2-3 days per week.'),
(9, 4, 'daily.'),
(9, 5, 'more than 3 times daily.'),
(10, 1, 'none this year.'),
(10, 2, 'once this year.'),
(10, 3, '2-3 times this year.'),
(10, 4, 'once during the past 6 months.'),
(10, 5, 'more than three times during the past 6 months.'),
(11, 1, 'at no point (never).'),
(11, 2, 'once only.'),
(11, 3, 'more than once during the ownership of the game.'),
(11, 4, 'once during the session.'),
(11, 5, 'more than three times during the session.'),
(12, 1, 'at no point (never).'),
(12, 2, '1-5 times.'),
(12, 3, '6-10 times.'),
(12, 4, '11-20 times.'),
(12, 5, 'more than 20 times.'),
(13, 1, 'never.'),
(13, 2, 'monthly.'),
(13, 3, 'weekly.'),
(13, 4, 'daily.'),
(13, 5, 'more than 3 times daily.'),
(14, 1, 'none this year.'),
(14, 2, 'once this year.'),
(14, 3, '2-3 times this year.'),
(14, 4, 'once during the past 6 months.'),
(14, 5, 'more than three times during the past 6 months.'),
(15, 1, 'none this year.'),
(15, 2, 'once this year.'),
(15, 3, '2-3 times this year.'),
(15, 4, 'once during the past 6 months.'),
(15, 5, 'more than three times during the past 6 months.'),
(16, 1, 'not at all.'),
(16, 2, 'weekly.'),
(16, 3, '2-3 days per week.'),
(16, 4, 'daily.'),
(16, 5, 'more than 3 times per day.'),
(17, 1, 'not at all.'),
(17, 2, 'weekly.'),
(17, 3, '2-3 days per week.'),
(17, 4, 'daily.'),
(17, 5, 'more than 3 times per day.'),
(18, 1, 'not at all.'),
(18, 2, '1-5 times per day.'),
(18, 3, '6-10 times per day.'),
(18, 4, '11-20 times per day.'),
(18, 5, '16 or more times per day.'),
(19, 1, '0% of the time.'),
(19, 2, '25% of the time.'),
(19, 3, '50% of the time.'),
(19, 4, '75% of the time.'),
(19, 5, '100% of the time.'),
(20, 1, '0% of the time.'),
(20, 2, '25% of the time.'),
(20, 3, '50% of the time.'),
(20, 4, '75% of the time.'),
(20, 5, '100% of the time.'),
(21, 1, 'not at all.'),
(21, 2, 'weekly.'),
(21, 3, '2-3 days per week.'),
(21, 4, 'daily.'),
(21, 5, 'more than 3 times per day.'),
(22, 1, 'not at all.'),
(22, 2, 'weekly.'),
(22, 3, '2-3 days per week.'),
(22, 4, 'daily.'),
(22, 5, 'more than 3 times per day.'),
(23, 1, 'not at all.'),
(23, 2, 'weekly.'),
(23, 3, '2-3 days per week.'),
(23, 4, 'daily.'),
(23, 5, 'more than 3 times per day.'),
(24, 1, 'not at all.'),
(24, 2, 'weekly.'),
(24, 3, '2-3 days per week.'),
(24, 4, 'daily.'),
(24, 5, 'more than 3 times per day.'),
(25, 1, 'at no point (never).'),
(25, 2, '1-5 times.'),
(25, 3, '6-10 times.'),
(25, 4, '11-20 times.'),
(25, 5, 'more than 20 times.'),
(26, 1, 'at no point (never).'),
(26, 2, '1-5 times.'),
(26, 3, '6-10 times.'),
(26, 4, '11-20 times.'),
(26, 5, 'more than 20 times.'),
(27, 1, 'at no point (never).'),
(27, 2, '1-5 times.'),
(27, 3, '6-10 times.'),
(27, 4, '11-20 times.'),
(27, 5, 'more than 20 times.'),
(28, 1, 'not at all.'),
(28, 2, 'weekly.'),
(28, 3, '2-3 days per week.'),
(28, 4, 'daily.'),
(28, 5, 'more than 3 times per day.'),
(29, 1, 'at no time during the week.'),
(29, 2, 'weekly.'),
(29, 3, '2-3 days per week.'),
(29, 4, 'daily.'),
(29, 5, 'more than 3 times per day.'),
(30, 1, 'at no time during the week.'),
(30, 2, 'weekly.'),
(30, 3, '2-3 days per week.'),
(30, 4, 'daily.'),
(30, 5, 'more than 3 times daily.'),
(31, 1, 'at no point (never).'),
(31, 2, 'weekly.'),
(31, 3, '2-3 days per week.'),
(31, 4, 'daily.'),
(31, 5, 'more than 3 times daily.'),
(32, 1, 'none at all'),
(32, 2, '1'),
(32, 3, '2'),
(32, 4, '3'),
(32, 5, '4 or more'),
(33, 1, 'at no time during the week.'),
(33, 2, 'weekly.'),
(33, 3, '2-3 days per week.'),
(33, 4, 'daily.'),
(33, 5, 'more than 3 times daily.'),
(34, 1, 'none at all'),
(34, 2, '1'),
(34, 3, '2'),
(34, 4, '3'),
(34, 5, '4 or more'),
(35, 1, 'none at all'),
(35, 2, '1-5 times per year.'),
(35, 3, '6-10 times per year.'),
(35, 4, '11-20 times per year.'),
(35, 5, 'more than 15 times per year.'),
(36, 1, '$0-9,999'),
(36, 2, '$10,000-19,999'),
(36, 3, '$20,000-39,999'),
(36, 4, '$40,000-59,000'),
(36, 5, '$60,000 or more'),
(37, 1, 'none at all'),
(37, 2, '1'),
(37, 3, '2'),
(37, 4, '3'),
(37, 5, '4 or more'),
(38, 1, '1'),
(38, 2, '2'),
(38, 3, '3'),
(38, 4, '4'),
(38, 5, '5 or more'),
(39, 1, 'not at all.'),
(39, 2, 'less than an hour.'),
(39, 3, 'approximately 1-2 hours.'),
(39, 4, 'approximately 3-5 hours.'),
(39, 5, 'all day long.'),
(40, 1, '50 and over'),
(40, 2, '40-49'),
(40, 3, '30-39'),
(40, 4, '20-29'),
(40, 5, '18-19'),
(41, 1, 'male.'),
(41, 2, 'female.'),
(41, 3, 'transgender.'),
(42, 1, 'Strongly agree'),
(42, 2, 'Agree'),
(42, 3, 'Neutral'),
(42, 4, 'Disagree'),
(42, 5, 'Strongly disagree'),
(43, 1, 'Strongly agree'),
(43, 2, 'Agree'),
(43, 3, 'Neutral'),
(43, 4, 'Disagree'),
(43, 5, 'Strongly disagree'),
(44, 1, 'Strongly agree'),
(44, 2, 'Agree'),
(44, 3, 'Neutral'),
(44, 4, 'Disagree'),
(44, 5, 'Strongly disagree'),
(45, 1, 'Strongly agree'),
(45, 2, 'Agree'),
(45, 3, 'Neutral'),
(45, 4, 'Disagree'),
(45, 5, 'Strongly disagree'),
(46, 1, 'Strongly agree'),
(46, 2, 'Agree'),
(46, 3, 'Neutral'),
(46, 4, 'Disagree'),
(46, 5, 'Strongly disagree')");

  db_query("INSERT INTO {dpistudy_keys} (invite_key, notes) VALUES
('test', 'This is just for testing before the proposal is accepted.'),
('practice', 'Another testing key.'),
('drupal', 'Contacts who have a connection to the Drupal content management system.'),
('ugrd', 'Undergraduate students at University of Central Florida.'),
('grad', 'Graduate students at University of Central Florida.'),
('aa', 'Contacts who have a business relationship with Advanced Automation, Inc, in Lindale, TX.'),
('cg', 'Contacts who employees of Classic Graphics in Charlotte, NC.'),
('d', 'People referred from clicking a link on deekayen.net.'),
('g', 'Visitors of a site that clicked a Google AdWords banner.'),
('tfsbo', 'Customers of Timeshares for Sale by Owner, Orlando, FL.'),
('fb', 'People who clicked the questionnaire invitation on facebook.com.'),
('ms', 'People who clicked the questionnaire invitation on myspace.com.'),
('friend', 'Personal friend of David Norman.')");

  db_query("INSERT INTO {dpistudy_questions} (pkid, weight, title) VALUES
(1, 2, 'I communicate with others using email'),
(2, 3, 'I communicate with others using instant messaging (IM)'),
(3, 4, 'I communicate with others using chat rooms'),
(4, 6, 'I read or contribute to Web blogs'),
(5, 7, 'I share images and pictures online'),
(6, 8, 'I share ideas, papers, information, and knowledge online'),
(7, 9, 'I make online purchases'),
(8, 10, 'I download music from the Internet'),
(9, 11, 'I download movies from the Internet'),
(10, 13, 'I have updated my website or personal web space (e.g. MySpace)'),
(11, 14, 'When playing video games, I customize the characters or scenes within the game'),
(12, 15, 'I initially meet or arrange meetings with new people online'),
(13, 16, 'I meet with people online'),
(14, 17, 'I have downloaded MP3 files from the Internet'),
(15, 18, 'I have downloaded videos and images from the Internet'),
(16, 19, 'I use email or the Internet to complete group assignments for school and/or work'),
(17, 20, 'I participate in group games (MMORPGS)'),
(18, 21, 'I use a portable digital assistant (PDA) (e.g. PocketPC, PalmPilot, Blackberry)'),
(19, 22, 'I review online evaluation systems (e.g. star rating system) before making online purchases'),
(20, 23, 'I contribute to online evaluation systems (e.g. star rating system) after making online purchases'),
(21, 24, 'I play video games'),
(22, 25, 'I play 1-2 player video games'),
(23, 26, 'I play games requiring more than 2 players'),
(24, 27, 'I use handheld game devices'),
(25, 28, 'I have taken courses online'),
(26, 29, 'I found information to complete school or work assignments online'),
(27, 30, 'I have gone online to learn about topics that interest me'),
(28, 32, 'I use the internet to communicate with the instructor, fellow classmates, or coworkers'),
(29, 33, 'I use search engines to locate information on the Internet'),
(30, 34, 'I use filtering tools (advanced search, directories, etc.) when locating information on the Internet'),
(31, 35, 'I search for information for entertainment and other personal reasons online'),
(32, 36, 'I have expertise in the following number of programming languages'),
(33, 37, 'I socialize with others online'),
(34, 38, 'When I am online, I can manage the following maximum number of conversations at the same time'),
(35, 39, 'I travel for business'),
(36, 40, 'My family''s annual gross income is'),
(37, 41, 'I have the following number of computers in my home'),
(38, 42, 'I have the following number of people in my household'),
(39, 43, 'I use a computer at work'),
(40, 44, 'My age group is'),
(41, 45, 'My gender is'),
(42, 46, 'I prefer training and/or educational materials that present graphics, rather than text first'),
(43, 47, 'I prefer training and/or education that allows me to randomly access various components of a lesson, rather than materials that step me through a lesson one component at a time'),
(44, 48, 'I prefer to complete multiple tasks (e.g. Instant Messaging, alternative activities, watching TV) rather than one task at a time while I''m learning'),
(45, 49, 'I prefer training and/or education that is play oriented, rather than work oriented'),
(46, 50, 'I prefer training and/or education that encourages me to communicate and learn with others rather than learning by myself')");

  variable_set('site_frontpage', 'dpistudy');

  variable_set('dpistudy_irb_text', '<p>A research project is being conducted by Mr. David Norman to study the impact of digital media in daily life. The purpose of this study is to observe whether there is any relationship between digital media interaction and student performance with different forms of instruction.</p>

<p>You are being asked to take part in this study by completing a questionnaire. Please be aware that you are not required to participate in this research and you may discontinue your participation at any time without penalty. You may also omit any items on the questionnaire you prefer not to answer.</p>

<p>There are no risks associated with participation in this study. Your responses will be analyzed and reported anonymously to protect your privacy.</p>

<p>Potential benefits associated with the study include finding new ways to teach students using different approaches based on how much they tend to use technology. There are no direct benefits or compensation for your participation.</p>

<p>Research at the University of Central Florida involving human participants is carried out under the oversight of the Institutional Review Board. Questions or concerns about research participants\' rights may be directed to the UCF IRB office, University of Central Florida, Office of Research & Commercialization, 12201 Research Parkway, Suite 501, Orlando, FL 32826-3246, or by campus mail 32816-0150. The hours of operation are 8:00 am until 5:00 pm, Monday through Friday except on University of Central Florida official holidays. The telephone numbers are (407) 882-2276 and (407) 823-2901.</p>

<p>For any other questions about this study, contact David Norman at <a href="&#x6d;&#97;&#x69;&#108;&#x74;&#x6f;&#x3a;&#x64;&#97;&#51;&#x31;&#54;&#51;&#48;&#x35;&#64;&#112;&#x65;&#x67;&#x61;&#115;&#x75;&#115;&#46;&#99;&#x63;&#x2e;&#x75;&#99;&#102;&#x2e;&#x65;&#100;&#117;">&#x64;&#97;&#51;&#x31;&#54;&#51;&#48;&#x35;&#64;&#112;&#x65;&#x67;&#x61;&#115;&#x75;&#115;&#46;&#99;&#x63;&#x2e;&#x75;&#99;&#102;&#x2e;&#x65;&#100;&#117;</a>. This research is being conducted with Dr. Atsusi Hirumi (<a href="&#109;&#97;&#105;&#x6c;&#x74;&#111;&#58;&#104;&#105;&#114;&#117;&#x6d;&#x69;&#x40;&#x6d;&#97;&#105;&#108;&#x2e;&#x75;&#99;&#x66;&#x2e;&#x65;&#100;&#117;">&#104;&#105;&#114;&#117;&#x6d;&#x69;&#x40;&#x6d;&#97;&#105;&#108;&#x2e;&#x75;&#99;&#x66;&#x2e;&#x65;&#100;&#117;</a>), Associate Professor & Co Chair of Instructional Technology at the University of Central Florida.

<p>By participating in this survey you voluntarily agree to allow the researchers to use the information you provide for related presentations, publications, and future research. If you decide to participate in this research study, you must be at least 18 years old and click "I consent" at the bottom of this screen.</p>');

  variable_set('dpistudy_score_screen', '<p>Your Digital Propensity Index score is <strong>@score</strong>. A score of 34 would represent the far extreme Digital Immigrant as opposed to the other extreme of 170 for a pure Digital Native.</p>

<p>According to <a href="http://www.marcprensky.com/">Marc Prensky</a>, Digital Natives are more likely to be used to receiving information quickly, like to parallel process and multi-task, prefer their graphics before text, and prefer random access like hypertext you see on websites. They function best when networked, thrive on instant gratification and rewards, and like games.</p>

<p>Digital Immigrants commonly did not grow up with digital media. They prefer linear, step-by-step tasks, perform best when doing one thing at a time, and are used to receiving information slowly. A comparison of Digital Natives and Digital Immigrants is as follows:</p>

<table border="1">
<tr><th>Digital Immigrants</th><th>Digital Natives</th></tr>
<tr><td valign="top">
<ul>
<li>Merely adopted aspects of new technologies</li>
<li>Turn to the Internet second rather than first</li>
<li>Print emails</li>
<li>Bring people physically to view websites</li>
<li>Call to confirm email receipt</li>
<li>Used to receiving information slow</li>
<li>Prefer text before graphics</li>
<li>Prefer linear, step-by-step tasks</li>
<li>Prefer serious work to games</li>
<li>Function best by doing one thing at a time, individually</li>
</ul>
</td><td valign="top">
<ul>
<li>"Spent entire lives surrounded by and using computers, videogames, digital music players, video cams, cell phones..."</li>
<li>"Process information differently from predecessors..."</li>
<li>Used to receiving information fast</li>
<li>Like to parallel and multi-task</li>
<li>Prefer graphics before text</li>
<li>Prefer random access</li>
<li>Function best when networked</li>
<li>Thrive on instant gratification and frequent rewards</li>
<li>Prefer games to serious work</li>
<li>Have short attention spans</li>
</ul>
</td></tr>
</table>

<p>For more information about Digital Immigrants and Digital Natives, read "<a href="http://www.marcprensky.com/writing/default.asp">Digital Natives, Digital Immigrants</a>" by Marc Prensky.</p>');
}

/**
 * Implementation of hook_uninstall().
 */
function dpistudy_uninstall() {
  drupal_uninstall_schema('dpistudy');

  variable_set('site_frontpage', 'node');
  variable_del('dpistudy_irb_text');
  variable_del('dpistudy_irb_consent_active');
  variable_del('dpistudy_add_form_num_questions');
  variable_del('dpistudy_alt_irb_url');
  variable_del('dpistudy_score_screen');
  cache_clear_all('dpistudy-', 'cache', TRUE);
}