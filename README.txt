ABOUT
-----

Distribution of the Digital Propensity Index questionnaire and the
instructional intervention for David Kent Norman's dissertation
in partial fulfillment of the requirements for the degree of Doctor
of Philosophy in Instructional Technology in the Department of
Educational Research, Technology, and Leadership in the College of
Education at the University of Central Florida, Orlando, Florida.

This is a module for the Drupal content management system, hosted
at http://drupal.org. It is for use with version Drupal 6.x.
Though Drupal 6.x operates correctly using PHP4, certain components
in this module require features first introduced in PHP5.

AUTHOR
------

David Kent Norman
http://deekayen.net/