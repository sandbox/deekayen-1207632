var timerID = null;
var timerRunning = false;
var startDate;
var startSecs;

function stopclock() {
  if(timerRunning) {
    clearTimeout(timerID);
  }
  timerRunning = false;
}

function startclock() {
  if(timerRunning == false) {
    startDate = new Date();
    startSecs = (startDate.getHours()*60*60) + (startDate.getMinutes()*60) + startDate.getSeconds();
  }

  stopclock();
  showtime();
}

function showtime() {
  var now = new Date();
  var nowSecs = (now.getHours()*60*60) + (now.getMinutes()*60) + now.getSeconds();
  var elapsedSecs = nowSecs - startSecs;;

  var hours = Math.floor( elapsedSecs / 3600 );
  elapsedSecs = elapsedSecs - (hours*3600);

  var minutes = Math.floor( elapsedSecs / 60 );
  elapsedSecs = elapsedSecs - (minutes*60);

  var seconds = elapsedSecs;

  var timeValue = "" + hours;
  timeValue += ((minutes < 10) ? ":0" : ":") + minutes;
  timeValue += ((seconds < 10) ? ":0" : ":") + seconds;

  document.dpistudy.js_timer.value = timeValue;
  timerID = setTimeout("showtime()",1000);
  timerRunning = true;
}

function dpistudy_q5(center) {
  stopclock();
  document.dpistudy.instruct_q5.value = center;
  document.dpistudy.submit();
}